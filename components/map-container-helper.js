// Phaser fails to load in server environment
const MapContainer = process.browser
  ? require('./map-container.vue').default
  : null

export default MapContainer
