import Phaser from 'phaser'
import { SCALING_FACTOR } from './constants'
import {
  setMapData,
  setPlayerPosition,
  setStartPosition,
  useDoor,
  interaction
} from './custom-events'

const { DOWN, LEFT, RIGHT, UP } = Phaser.Math.Vector2
const directions = { DOWN, LEFT, RIGHT, UP }

const speed = 300

const isInteractiveLayer = layerData =>
  ['SALESMAN_', 'BARRIER_', 'SWITCH_'].some(
    prefix => layerData.name.indexOf(prefix) === 0
  )

const isBlockingLayer = layerData => {
  const { properties } = layerData
  const blockedProperty = Object.values(properties).find(
    property => property.name === 'blocked'
  )
  return (blockedProperty && blockedProperty.value) || false
}

class MapScene extends Phaser.Scene {
  constructor({ baseUrl, mapFilePath, debugFlags, playerState, mapState }) {
    super({ key: mapFilePath })
    Object.assign(this, {
      baseUrl,
      mapFilePath,
      debugFlags,
      playerState,
      mapState
    })
  }

  animationKey(directionKey) {
    const { characterName } = this.playerState
    return `characters/${characterName}_${directionKey.toLowerCase()}`
  }

  tilesetCacheKey(name) {
    return `${this.mapFilePath}/tilesets/${name}`
  }

  preload() {
    this.events.emit(setMapData, null)
    this.load.setBaseURL(this.baseUrl)

    this.load.on('filecomplete', (key, type, data) => {
      if (key !== this.mapFilePath) {
        return
      }

      const { data: mapData } = data
      const { tilesets } = mapData
      tilesets.forEach(({ image, name }) => {
        const tilesetUrl = image.replace(/^\.\.\//, '') // this relies on maps/ and tilesets/ directory being at the same level
        this.load.image(this.tilesetCacheKey(name), tilesetUrl)
      })
    })

    this.load.tilemapTiledJSON(this.mapFilePath, this.mapFilePath)

    Object.keys(directions).forEach(directionKey =>
      this.load.atlas(this.animationKey(directionKey))
    )
  }

  create() {
    this.map = this.make.tilemap({ key: this.mapFilePath })

    const { tileWidth, tileHeight } = this.map
    const startPositionObject = this.map.findObject(
      'doors',
      object => object.name === 'start_position'
    )
    if (startPositionObject) {
      this.events.emit(setStartPosition, {
        x: startPositionObject.x / tileWidth,
        y: startPositionObject.y / tileHeight
      })
    }

    const { data: mapData } = this.cache.tilemap.get(this.mapFilePath)
    this.events.emit(setMapData, mapData)

    const { tilesets } = mapData
    const tilesetNames = tilesets.map(({ name }) => name)
    tilesetNames.forEach(name =>
      this.map.addTilesetImage(name, this.tilesetCacheKey(name))
    )

    const tileLayerNames = mapData.layers
      .filter(layer => layer.type === 'tilelayer')
      .map(layer => layer.name)
    this.tileLayers = tileLayerNames
      .map(name => {
        if (name === 'PLAYER' && this.playerState.tilePosition) {
          const { tilePosition } = this.playerState
          const playerPosition = this.map.tileToWorldXY(
            tilePosition.x,
            tilePosition.y
          )
          this.player = this.add.sprite(
            playerPosition.x,
            playerPosition.y,
            this.animationKey('down')
          )
          this.player.setScale(SCALING_FACTOR)
          this.player.setOrigin(1 / 3)
          return null
        }

        if (this.mapState.hiddenLayers.indexOf(name) > -1) {
          return null
        }

        const layer = this.map.createDynamicLayer(name, tilesetNames, 0, 0)
        layer.setScale(SCALING_FACTOR)
        return layer
      })
      .filter(layer => layer !== null)

    this.cursors = this.input.keyboard.createCursorKeys()

    const camera = this.cameras.main
    if (this.player) {
      camera.startFollow(this.player)
    }
    camera.setBounds(
      0,
      0,
      SCALING_FACTOR * this.map.widthInPixels,
      SCALING_FACTOR * this.map.heightInPixels
    )

    Object.keys(directions).forEach(directionKey => {
      const key = this.animationKey(directionKey)
      this.anims.create({
        key,
        frames: this.anims.generateFrameNames(key),
        duration: speed
      })
    })
  }

  isBlocked(tileX, tileY) {
    let hasTile = false

    const hasBlockingLayer = this.tileLayers.some(layer => {
      const tile = layer.getTileAt(tileX, tileY)
      if (tile) {
        hasTile = true
      } else {
        return false
      }

      const { layer: layerData } = layer
      return isBlockingLayer(layerData) && !this.debugFlags.ignoreBlockingTiles
    })

    return hasBlockingLayer || !hasTile
  }

  findInteractiveTile(tilePosition) {
    for (let i = 0; i < this.tileLayers.length; i++) {
      const layer = this.tileLayers[i]
      const { layer: layerData } = layer
      if (!isInteractiveLayer(layerData)) {
        continue
      }

      const tile = layer.getTileAt(tilePosition.x, tilePosition.y)
      if (tile) {
        return tile
      }
    }

    return null
  }

  update(time, delta) {
    if (
      !this.player ||
      this.tweens.isTweening(this.player) ||
      this.mapState.hasOverlay()
    ) {
      return
    }

    const [directionKey, direction] =
      Object.entries(directions).find(
        ([key]) => this.cursors[key.toLowerCase()].isDown
      ) || []

    if (!directionKey) {
      return
    }

    this.player.play(this.animationKey(directionKey))

    const newTilePosition = new Phaser.Math.Vector2(
      this.playerState.tilePosition
    )
    newTilePosition.add(direction)

    const { tileWidth, tileHeight } = this.map
    const door = this.map.findObject(
      'doors',
      object =>
        newTilePosition.x === object.x / tileWidth &&
        newTilePosition.y === object.y / tileHeight
    )

    if (door && door.name !== 'start_position') {
      this.events.emit(useDoor, Object.values(door.properties))
    }

    const interactiveTile = this.findInteractiveTile(newTilePosition)
    if (interactiveTile) {
      this.events.emit(interaction, interactiveTile.layer.name)
      if (!this.debugFlags.ignoreBlockingTiles) {
        return
      }
    }

    if (this.isBlocked(newTilePosition.x, newTilePosition.y)) {
      return
    }

    const newPosition = this.map.tileToWorldXY(
      newTilePosition.x,
      newTilePosition.y
    )

    this.tweens.add({
      targets: this.player,
      x: newPosition.x,
      y: newPosition.y,
      duration: speed,
      onComplete: () => {
        this.events.emit(setPlayerPosition, newTilePosition)
      }
    })
  }

  hideLayers(layerNames) {
    this.tileLayers = this.tileLayers.filter(layer => {
      const { layer: layerData } = layer

      if (layerNames.indexOf(layerData.name) > -1) {
        layer.destroy()
        return false
      }

      return true
    })
  }
}

export default MapScene
