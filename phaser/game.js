import Phaser from 'phaser'
import { SCALING_FACTOR } from './constants'

class Game extends Phaser.Game {
  constructor(options) {
    super({
      width: 15 * 16 * SCALING_FACTOR,
      height: 12 * 16 * SCALING_FACTOR,
      type: Phaser.AUTO,
      scene: {},
      transparent: true,
      antialias: false,
      physics: {
        default: 'arcade',
        arcade: {
          gravity: { y: 0 } // Top down game, so no gravity
        }
      },
      ...options
    })
  }
}

export default Game
