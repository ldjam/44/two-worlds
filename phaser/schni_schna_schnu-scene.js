import Phaser from 'phaser'
import { Howl } from 'howler'

class SchniSchnaSchnu extends Phaser.Scene {
  constructor({ baseUrl, key, endFight, scissorLevel, paperLevel, rockLevel }) {
    super({ key })

    this.myHp = 100
    this.enemyHP = 100

    this.scissorLevel = scissorLevel
    this.paperLevel = paperLevel
    this.rockLevel = rockLevel

    this.postionScissor = [220, 200]
    this.positionPaper = [360, 400]
    this.positionStone = [500, 200]

    this.myAction = ''
    this.enemyAction = ''
    this.baseUrl = baseUrl
    this.endFight = endFight

    this.clickSound = new Howl({
      src: ['sounds/click.ogg', 'sounds/click.mp3']
    })
  }

  preload() {
    this.load.setBaseURL(this.baseUrl)
    this.load.image('SchniSchnaSchnuPaper', 'fight_images/paper2.png')
    this.load.image('SchniSchnaSchnuScissors', 'fight_images/scissors.png')
    this.load.image('SchniSchnaSchnuStone', 'fight_images/stone.png')
    this.load.image('SchniSchnaSchnuBg', 'fight_images/hintergrund2.png')

    this.load.image('chosenObject', 'fight_images/chosen_object.png')
  }

  create(data) {
    this.myHpScene = this.scene.get('myHpScene')
    this.enemyHpScene = this.scene.get('enemyHpScene')

    this.SchniSchnaSchnuBg = this.add.image(0, 0, 'SchniSchnaSchnuBg')
    this.SchniSchnaSchnuBg.setOrigin(0)

    this.SchniSchnaSchnuPaper = this.add
      .image(
        this.positionPaper[0],
        this.positionPaper[1],
        'SchniSchnaSchnuPaper'
      )
      .setInteractive()
      .setData({ type: 'paper' })

    this.SchniSchnaSchnuScissors = this.add
      .image(
        this.postionScissor[0],
        this.postionScissor[1],
        'SchniSchnaSchnuScissors'
      )
      .setInteractive()
      .setData({ type: 'scissor' })

    this.SchniSchnaSchnuStone = this.add
      .image(
        this.positionStone[0],
        this.positionStone[1],
        'SchniSchnaSchnuStone'
      )
      .setInteractive()
      .setData({ type: 'rock' })

    this.chosenObject = this.add
      .image(500, 200, 'chosenObject')
      .setInteractive()
      .setVisible(false)
      .setData({ type: 'rock' })

    this.input.on('gameobjectup', this.clickHandler, this)
  }

  clickHandler(pointer, object) {
    if (localStorage && localStorage.isSoundEnabled !== 'false') {
      this.clickSound.play()
    }

    this.myAction = object.getData('type')

    if (this.myAction === 'rock') {
      this.chosenObject.setPosition(
        this.positionStone[0],
        this.positionStone[1]
      )
      this.chosenObject.setVisible(true)
    }

    if (this.myAction === 'paper') {
      this.chosenObject.setPosition(
        this.positionPaper[0],
        this.positionPaper[1]
      )
      this.chosenObject.setVisible(true)
    }

    if (this.myAction === 'scissor') {
      this.chosenObject.setPosition(
        this.postionScissor[0],
        this.postionScissor[1]
      )
      this.chosenObject.setVisible(true)
    }

    this.enemyDoAction()

    setTimeout(() => this.evaluateAction(), 200)
  }

  enemyDoAction() {
    // generates a random number between 0-2
    const randomNum = Math.floor(Math.random() * 3)

    // generate a random number and assign it to one of the 3 choices
    if (randomNum === 0) {
      this.enemyAction = 'rock'
    } else if (randomNum === 1) {
      this.enemyAction = 'paper'
    } else {
      this.enemyAction = 'scissor'
    }
    console.log('enemy Chooses ' + this.enemyAction)
  }

  evaluateAction() {
    // user chooses rock
    if (this.myAction === this.enemyAction) {
      this.resultsTie()
    } else if (this.myAction === 'rock' && this.enemyAction === 'paper') {
      this.resultsLoser()
    } else if (this.myAction === 'rock' && this.enemyAction === 'scissor') {
      this.resultsWinner()
    }

    // user chooses paper
    if (this.myAction === 'paper' && this.enemyAction === 'rock') {
      this.resultsWinner()
    } else if (this.myAction === 'paper' && this.enemyAction === 'scissor') {
      this.resultsLoser()
    }

    // user chooses scissors
    if (this.myAction === 'scissor' && this.enemyAction === 'rock') {
      this.resultsLoser()
    } else if (this.myAction === 'scissor' && this.enemyAction === 'paper') {
      this.resultsWinner()
    }

    this.chosenObject.setVisible(false)

    if (this.myHp <= 0 || this.enemyHP <= 0) {
      this.SchniSchnaSchnuPaper.destroy()
      this.SchniSchnaSchnuScissors.destroy()
      this.SchniSchnaSchnuStone.destroy()

      this.endFight(this.enemyHP <= 0)
    }
  }

  resultsTie() {
    console.log('tie')
  }

  resultsWinner() {
    console.log('winner')
    if (this.myAction === 'scissor') {
      this.enemyHP = this.enemyHP - this.scissorLevel * 25
    }

    if (this.myAction === 'paper') {
      this.enemyHP = this.enemyHP - this.paperLevel * 25
    }

    if (this.myAction === 'rock') {
      this.enemyHP = this.enemyHP - this.rockLevel * 25
    }

    this.enemyHpScene.setCurrentValue(this.enemyHP)
    this.enemyHpScene.update()
  }

  resultsLoser() {
    console.log('loser')
    this.myHp = this.myHp - 25
    this.myHpScene.setCurrentValue(this.myHp)
    this.enemyHpScene.update()
  }
}

export default SchniSchnaSchnu
