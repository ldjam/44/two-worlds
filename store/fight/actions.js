export default {
  setEnemyName({ commit }, enemyName) {
    commit('SET_ENEMY_NAME', enemyName)
  }
}
