export default {
  hasOverlay(state) {
    return state.overlay !== null
  }
}
