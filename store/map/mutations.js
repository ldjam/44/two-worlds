const findProperty = (mapData, key) => {
  if (!mapData.properties) {
    return null
  }

  return Object.values(mapData.properties).find(
    property => property.name === key
  )
}

export default {
  SET_MAP_FILE(state, { newMapFilePath, routeName }) {
    state.mapFilePath = newMapFilePath

    if (routeName === 'light-world') {
      state.lightMapFilePath = newMapFilePath
    } else if (routeName === 'shadow-world') {
      state.shadowMapFilePath = newMapFilePath
    }
  },

  SET_MAP_DATA(state, newData) {
    state.mapData = newData

    if (!newData) {
      return
    }

    const shadowMap = findProperty(newData, 'shadow-map')
    if (shadowMap) {
      state.shadowMapFilePath = `maps/${shadowMap.value}.json`
    }

    const lightMap = findProperty(newData, 'light-map')
    if (lightMap) {
      state.lightMapFilePath = `maps/${lightMap.value}.json`
    }
  },

  SET_OVERLAY(state, overlay) {
    state.overlay = overlay
  },

  HIDE_LAYER(state, layerName) {
    state.hiddenLayers.push(layerName)
  }
}
