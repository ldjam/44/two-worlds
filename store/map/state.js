export default () => ({
  mapFilePath: null,
  mapData: null,
  overlay: null,
  shadowMapFilePath: 'maps/chamber_of_the_princess.json',
  lightMapFilePath: 'maps/house_of_sun.json',
  hiddenLayers: []
})
