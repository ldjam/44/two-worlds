import playSound from '~/helpers/play-sound'

export default {
  setMapFilePath({ commit }, newMapFilePath) {
    const { router } = this.app
    const { name: routeName } = router.currentRoute
    commit('SET_MAP_FILE', { newMapFilePath, routeName })
  },

  setMapData({ commit }, newData) {
    commit('SET_MAP_DATA', newData)
  },

  setOverlay({ commit }, overlay) {
    commit('SET_OVERLAY', overlay)
  },

  hideLayer({ commit }, layerName) {
    commit('HIDE_LAYER', layerName)
    playSound('pling')
  }
}
