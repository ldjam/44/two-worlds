export default {
  activate({ dispatch }, { routeName, layerName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Citizen: Here you can buy the Eye of the Death Gods. With that you can see the remaining lifetime.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'Ok…' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  }
}
