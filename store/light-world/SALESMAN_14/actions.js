export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: Oh hello! Are you interested in our company? Learn more for only 3 years! ",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'Hm, I\'m not interested.' },
          { name: `${routeName}/${layerName}/interest`, text: "I\'m interested."}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  interest({ dispatch }) {
    return Promise.all([
      dispatch(
        'map/setOverlay',
        {
          text: "We, the gods of death, have developed a concept that does not disadvantage people. " +
          "Anyone can buy our high quality products, whether poor or rich! " +
          "Because we do not ask for money! They simply pay with their lifetime. " +
          "And everyone has a lot of that, right?",
          actions: [
            {name: `light-world/SWITCH_1/ignore`, text: 'continue' }
          ]
        },
        { root: true }),
      dispatch('player/makeOlder', 3, { root: true } )

    ])
  }
}
