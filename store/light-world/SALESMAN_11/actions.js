export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: 'Salesman: Would you like a gourd?',
        actions: [
          {
            name: `${routeName}/${layerName}/acceptGourd`,
            text: 'Yes, please.',
            payload: { layerName, routeName }
          }
        ]
      },
      { root: true }
    )
  },

  acceptGourd({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: 'Salesman: Well, that makes 10 years of your life.',
        actions: [
          {
            name: `${routeName}/${layerName}/acceptPrice`,
            text: 'Ok.',
            payload: { layerName, routeName }
          }
        ]
      },
      { root: true }
    )
  },

  acceptPrice({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: 'Salesman: No, no, no. We need to haggle!',
        actions: [
          {
            name: `${routeName}/${layerName}/offer9Years`,
            text: 'Well then… 9 years?',
            payload: { layerName, routeName }
          }
        ]
      },
      { root: true }
    )
  },

  offer9Years({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: That's not how it works. You start with 5 years!",
        actions: [
          {
            name: `${routeName}/${layerName}/offer5Years`,
            text: 'Mhm. 5 years?',
            payload: { layerName, routeName }
          }
        ]
      },
      { root: true }
    )
  },

  offer5Years({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: 'Salesman: 5 years?! For this gourd? Are you insane?!',
        actions: [
          {
            name: `${routeName}/${layerName}/offer6Years`,
            text: "That's what you told me to say! 6 years?",
            payload: { layerName, routeName }
          }
        ]
      },
      { root: true }
    )
  },

  offer6Years({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text:
          'Salesman: Yeah, now you get it!… 6 years? I just sold one for twice as much!',
        actions: [
          {
            name: `${routeName}/${layerName}/offer8Years`,
            text: 'How about 8 years?',
            payload: { layerName, routeName }
          }
        ]
      },
      { root: true }
    )
  },

  offer8Years({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: 'Salesman: Done! Here is your gourd.',
        actions: [
          {
            name: `${routeName}/${layerName}/payGourd`,
            text: 'Thank you…',
            payload: { layerName, routeName }
          }
        ]
      },
      { root: true }
    )
  },

  payGourd({ dispatch }, { layerName }) {
    return Promise.all([
      dispatch('map/setOverlay', null, { root: true }),
      dispatch('player/makeOlder', 8, { root: true }),
      dispatch('map/hideLayer', layerName, { root: true })
    ])
  }
}
