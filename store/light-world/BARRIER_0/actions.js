export default {
  activate({ dispatch }, { routeName, layerName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Sun: A barrier. There seems to be a dark energy flowing through it.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  }
}
