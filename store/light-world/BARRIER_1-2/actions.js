export default {
  activate({ dispatch }, { routeName, layerName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Citizen: I want to know how long I have to live, that's why I'm standing here.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'Okay…' }
        ]
      },
      { root: true }
    )
  },
  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },


}
