export default {
  activate({ dispatch }, { routeName, layerName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Sun: General Loon! I can not get through here… Here are too many people.",
        actions: [
          { name: `${routeName}/${layerName}/speak`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  speak({ dispatch }) {
    dispatch('map/setOverlay',
      {
        text: "Loon: Argh. Then think and find a solution yourself! I'm busy here!",
        actions: [
          {name: "light-world/BARRIER_1-1/ignore", text: 'continue'}
        ],
      },
      { root: true }
    )
  }
}
