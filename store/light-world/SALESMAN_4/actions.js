export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Did you see? The car in the north blocks the whole street. " +
        "For 7 years of your life I would open this way for you.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'No thanks, I\'m looking for another way.' },
          { name: `${routeName}/${layerName}/buy`, text: 'Yes.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    dispatch('map/setOverlay', null, { root: true }).then(() =>
      dispatch('player/makeOlder', 7, { root: true })
    ).then(() => dispatch('map/hideLayer', "SALESMAN_4", { root: true }))
  }
}
