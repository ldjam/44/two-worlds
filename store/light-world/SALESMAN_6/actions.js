export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: Hey you, you look so exhausted. Try our vitamin drink with a new recipe! " +
        "Today for the trial price of 4 years of your life!",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'No thanks.' },
          { name: `${routeName}/${layerName}/buy`, text: 'Yes.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    dispatch('map/setOverlay',
      {
      text: "Salesman: Hihihihihi. (Humans are so naive…)",
      actions: [
        { name: `light-world/SALESMAN_6/speak`, text: 'continue' },
      ]
    }
    , { root: true }).then(() =>
      dispatch('player/makeOlder', 4, { root: true })
    ).then(() => dispatch('map/hideLayer', "SALESMAN_6", { root: true }))
  },

  speak({ dispatch}) {
    dispatch('map/setOverlay',
      {
        text: "Loon: Do you realize that, you've just been ripped off?",
        actions: [
          { name: `light-world/SALESMAN_6/ignore`, text: 'continue' },
        ]
      }
      , { root: true })
  }

}
