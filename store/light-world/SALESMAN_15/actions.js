export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Oh hello! I sell rubber boots here. " +
        "Everyone needs rubber boots! " +
        "I have red, yellow, striped, checkered ... " +
        "Today on sale! Only 2 years of your life!",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'No… thank you…' },
          { name: `${routeName}/${layerName}/buy`, text: 'I need one!'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    dispatch('map/setOverlay', null, { root: true }).then(() =>
      dispatch('player/makeOlder', 2, { root: true })
    )
  }
}
