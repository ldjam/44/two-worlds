export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: Hello, I can offer you scissors made of the best metal. " +
        "With 8 of your remaining time, you would help the General with his plan.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'He is strong. He doesn\'t need that.' },
          { name: `${routeName}/${layerName}/buy`, text: 'Yes, I want to help him.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    return Promise.all([
      dispatch('map/setOverlay', null, { root: true }),
      dispatch('player/makeOlder', 8, { root: true }),
      dispatch('player/upgrade', "scissor", {root: true})
    ]
    )
  }
}
