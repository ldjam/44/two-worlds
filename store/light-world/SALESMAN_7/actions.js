export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: May I give you a philosophical advice? It only costs you 13 years of life!",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'No thanks.' },
          { name: `${routeName}/${layerName}/buy`, text: 'Yes.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    dispatch('map/setOverlay',
      {
      text: "Salesman: A wise man once said: We only appreciate the time when we have not much left.",
      actions: [
        { name: `light-world/SALESMAN_7/loonSpeak`, text: 'continue' },
      ]
    }
    , { root: true }).then(() =>
      dispatch('player/makeOlder', 13, { root: true })
    ).then(() => dispatch('map/hideLayer', "SALESMAN_7", { root: true }))
  },

  loonSpeak( { dispatch})
  {
    dispatch('map/setOverlay',
      {
        text: "Loon: Really?! Did you sacrifice your life for this crap?! I don't understand humans.",
        actions: [
          { name: `light-world/SALESMAN_7/ignore`, text: 'continue' },
        ]
      },
      { root: true }
    )
  }
}
