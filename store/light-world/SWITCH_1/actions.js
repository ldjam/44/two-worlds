export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "That's a megaphone.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'Do not use.' },
          { name: `${routeName}/${layerName}/use`, text: 'Use.' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  use({ dispatch }) {
    return Promise.all([
      dispatch(
        'map/setOverlay',
        {
          text: "Sun: Here is an incredible special offer for decorative items! Only for a short time!",
        actions: [
          {name: `light-world/SWITCH_1/wonder`, text: 'continue' }
        ]
      },
        { root: true }),
      dispatch('map/hideLayer', "BARRIER_1-1", { root: true }),
      dispatch('map/hideLayer', "BARRIER_1-2", { root: true }),
      dispatch('map/hideLayer', "BARRIER_1-3", { root: true }),
      dispatch('map/hideLayer', "BARRIER_1", { root: true })

    ])
  },

  wonder({dispatch}){
    dispatch(
    'map/setOverlay',
      {
        text: "Loon: Oh, the barrier has disappeared.",
        actions: [
          { name: `light-world/SWITCH_1/ignore`, text: 'Do not use.' }
        ]
      },
      { root: true }
  )
  }

}
