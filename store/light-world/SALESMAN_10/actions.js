export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: I can offer you high quality paper. Chlorine-free bleached and from controlled cultivation." +
        "With 7 of your remaining time, you would help the General with his plan.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'He is strong. He doesn\'t need that.' },
          { name: `${routeName}/${layerName}/buy`, text: 'Yes, I want to help him.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    return Promise.all([
      dispatch('map/setOverlay', null, { root: true }),
      dispatch('player/makeOlder', 7, { root: true }),
      dispatch('player/upgrade', "paper", {root: true})
    ]
    )
  }
}
