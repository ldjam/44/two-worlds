export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: Ksch, ksch. I am currently in a purchase transaction. " +
        "His car is broken and blocks the way. " +
        "(Hihihi, I can probably sell my cartwheels for dear!)",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'continue!' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  }
}
