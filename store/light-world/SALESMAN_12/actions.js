export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: I have valuable information for you. For you, this costs 10 lifetime.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'I don\'t need them.' },
          {name: `${routeName}/${layerName}/buy`, text: 'Tell me more.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    dispatch('map/setOverlay',
      {
        text: "Salesman: I saw your sister being taken to the Sales Department office building ...",
        actions: [
          { name: `light-world/SALESMAN_12/answer`, text: 'continue' },
        ]
      }
      , { root: true })
  },

  answer({ dispatch}) {
    dispatch('map/setOverlay',
      {
        text: "Sun: Did you hear that? My sister and the princess are in this building.",
        actions: [
          { name: `light-world/SALESMAN_12/loon`, text: 'continue' },
        ]
      }
      , { root: true }
      ).then(() =>
      dispatch('player/makeOlder', 10, { root: true })
    ).then(() => dispatch('map/hideLayer', "SALESMAN_12", { root: true }))
  },

  loon({ dispatch}) {
    dispatch('map/setOverlay',
      {
        text: "Loon: \n" +
        "Yes, I'm not deaf! Let us in there.",
        actions: [
          {name: `light-world/SALESMAN_12/ignore`, text: 'continue'},
        ]
      }
      , {root: true})
  }
}

