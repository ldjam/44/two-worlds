export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Do you want to buy chocolate? It also costs only 1 lifetime.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'No, thanks.' },
          {name: `${routeName}/${layerName}/buy`, text: 'Buy.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    dispatch('map/setOverlay',
      {
        text: "You buy chocolate. You bought chocolate. You're a year older now and not prettier, but you're happy.",
        actions: [
          { name: `light-world/SALESMAN_9/ignore`, text: 'continue' },
        ]
      }
      , { root: true }).then(() =>
      dispatch('player/makeOlder', 1, { root: true })
    )
  },
}
