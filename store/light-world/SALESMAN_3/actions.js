export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Hey you! Do you want to know how long you will live? " +
        "Then I have just the right thing for you! " +
        "For 20 years of your life you will receive the eyes of the Death Gods! " +
        "With them you can see your remaining lifetime!",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'No, I do not want to know that!' },
          { name: `${routeName}/${layerName}/buy`, text: 'Yes, I would like to know how much longer I have to live!'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    return Promise.all([
      dispatch('map/setOverlay', null, { root: true }),
      dispatch('player/makeOlder', 20, { root: true }),
      dispatch('map/hideLayer', "SALESMAN_3", { root: true }),
      dispatch('player/showRemainingYears', null, { root: true })
    ])
  }
}
