export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: Do you want a heart for your loved one? " +
        "A proof of love from you will surely strengthen him in the shadow world.  " +
        "A heart costs you 5 years of your life.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'No, he\'s not nice. I hate him.' },
          { name: `${routeName}/${layerName}/buy`, text: 'I want to send him a heart.!'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    return Promise.all([
      dispatch('map/setOverlay',
        {
          text: "Loon: Eh ... what should this heart mean?",
          actions: [
            { name: `light-world/SALESMAN_13/ignore`, text: 'continue' }
          ]
        }
        , { root: true }),
      dispatch('player/makeOlder', 5, { root: true }),
      dispatch("player/increaseHearts", 1, {root: true})
    ]
    )
  }
}
