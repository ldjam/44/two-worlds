export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Death God: Oh, a customer. A pity, but I have nothing for you.",
        actions: [
          { name: `${routeName}/${layerName}/speak`, text: 'Continue' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  speak({ dispatch }) {
    dispatch('map/setOverlay',
      {
        text: "Sun: General Loon, the Death Gods will not let me move on. You have to defeat them in your world.",
        actions: [
          {name: "light-world/SALESMAN_2/continue", text: 'continue'}
        ],
      },
      { root: true }
    )
  },

  continue({ dispatch }) {
    dispatch('map/setOverlay',
      {
        text: "Loon: How annoying. You are not convenient.",
        actions: [
          {name: "light-world/SALESMAN_2/ignore", text: 'continue'}
        ],
      },
      { root: true }
    )
  },

}
