export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "This looks like an egg ... It emits dark energy. And there seems to be a switch on it..",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'Prefer not to touch.' },
          { name: `${routeName}/${layerName}/press`, text: 'Press switch' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  press({ dispatch }) {
    return Promise.all([
      dispatch(
        'map/setOverlay',
        {
          text: "Sun: Hm nothing happened. Has anything happened in the shadow world, General?",
        actions: [
          {name: `light-world/SWITCH_2/wonder`, text: 'continue' }
        ]
      },
        { root: true }),
      dispatch('map/hideLayer', "BARRIER_2", { root: true }),
    ])
  },

  wonder({dispatch}){
    dispatch(
    'map/setOverlay',
      {
        text: "Loon: The barrier in the shadow world has disappeared. Tss, you're good for something, Sun.",
        actions: [
          { name: `light-world/SWITCH_2/ignore`, text: 'continue' }
        ]
      },
      { root: true }
  )
  }

}
