export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: Hello, I can offer you stones from the best quarries in this country. " +
        "With 6 of your remaining time, you would help the General with his plan.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'He is strong. He doesn\'t need that.' },
          { name: `${routeName}/${layerName}/buy`, text: 'Yes, I want to help him.'}
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    return Promise.all([
      dispatch('map/setOverlay', null, { root: true }),
      dispatch('player/makeOlder', 6, { root: true }),
      dispatch('player/upgrade', "stone", {root: true})
    ]
    )
  }
}
