export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Salesman: May I offer you a new product from our Beauty Collection? " +
        "This is the new hand cream. It has not gotten better, but more expensive. " +
        "Now 5 years of life. (Oh damn ... I should not always tell the truth ...)",
        actions: [
          { name: `${routeName}/${layerName}/speakAgain`, text: 'No thanks.' },
          { name: `${routeName}/${layerName}/buy`, text: 'Buy.'}
        ]
      },
      { root: true }
    )
  },

  speakAgain({ dispatch}) {
    dispatch('map/setOverlay',
      {
        text: "Salesman: Really not? Please buy this cream, otherwise our king is angry with me.",
        actions: [
          { name: `light-world/SALESMAN_5/speak`, text: 'No, really not!' },
          { name: `light-world/SALESMAN_5/buy`, text: 'Buy.'}
        ]
      }
      , { root: true })
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  buy({ dispatch}) {
    dispatch('map/setOverlay',
      {
      text: "Loon: Why did you give your life time for something senseless? " +
      "I could have defeated him in my world. Pay attention to your time.",
      actions: [
        { name: `light-world/SALESMAN_5/ignore`, text: 'continue' },
      ]
    }
    , { root: true }).then(() =>
      dispatch('player/makeOlder', 5, { root: true })
    ).then(() => dispatch('map/hideLayer', "SALESMAN_5", { root: true }))
  },

  speak({ dispatch}) {
    dispatch('map/setOverlay',
      {
        text: "Salesman: Oh please! We have to keep our sales figures, otherwise something terrible will happen to me!",
        actions: [
          { name: `light-world/SALESMAN_5/ignore`, text: 'No' },
          { name: "light-world/SALESMAN_5/buy", text: "Alright, I will buy."}
        ]
      }
      , { root: true })
  }

}
