import playSound from '~/helpers/play-sound'
import { INVALID_POSITON } from './state'

const isInvalid = tilePosition =>
  tilePosition.x === INVALID_POSITON.x && tilePosition.y === INVALID_POSITON.y

export default {
  setStartPosition({ state, dispatch }, tilePosition) {
    if (!isInvalid(state.tilePosition)) {
      return
    }

    dispatch('setTilePosition', tilePosition)
  },

  setTilePosition({ commit }, tilePosition) {
    commit('SET_TILE_POSITION', tilePosition)
  },

  resetTilePosition({ commit }) {
    commit('SET_TILE_POSITION', INVALID_POSITON)
  },

  setCharacterName({ commit }, characterName) {
    commit('SET_CHARACTER_NAME', characterName)
  },

  makeOlder({ commit, state }, years) {
    commit('MAKE_OLDER', years)
    playSound('cash')

    if (state.age >= 80) {
      const { router } = this.app
      router.push('/game-over')
    }
  },

  reduceHearts({ commit, state }, numberOfLostHearts) {
    commit('REDUCE_HEARTS', numberOfLostHearts)
    playSound('oh')

    if (state.numberOfHearts < 1) {
      const { router } = this.app
      router.push('/game-over')
    }
  },

  increaseHearts({ commit, state }, numberOfBoughtHearts) {
    commit('INCREASE_HEARTS', numberOfBoughtHearts)
  },

  showRemainingYears({ commit }) {
    commit('SHOW_REMAINING_YEARS')
  },

  upgrade({ commit }, weapon) {
    commit('UPGRADE', weapon)
  }
}
