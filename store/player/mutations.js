export default {
  SET_TILE_POSITION(state, tilePosition) {
    Object.assign(state.tilePosition, tilePosition)
  },

  SET_CHARACTER_NAME(state, characterName) {
    state.characterName = characterName
  },

  MAKE_OLDER(state, years) {
    state.age += years
  },

  REDUCE_HEARTS(state, numberOfLostHearts) {
    state.numberOfHearts -= numberOfLostHearts
  },

  INCREASE_HEARTS(state, numberOfBoughtHearts) {
    state.numberOfHearts += numberOfBoughtHearts
  },

  SHOW_REMAINING_YEARS(state) {
    state.areRemainingYearsVisible = true
  },

  UPGRADE(state, weapon) {
    state[`${weapon}Level`] += 1
  }
}
