export const INVALID_POSITON = { x: -1, y: -1 }

export default () => ({
  tilePosition: INVALID_POSITON,
  characterName: null,
  age: 10,
  numberOfHearts: 4,
  areRemainingYearsVisible: false,
  scissorLevel: 1,
  paperLevel: 1,
  rockLevel: 1,
  startTime: Date.now()
})
