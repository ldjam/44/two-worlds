export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: So what is that? It emits a disgustingly bright light. Oh, a switch.",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'Do not press the button.' },
          { name: `${routeName}/${layerName}/press`, text: 'Press the button.' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  press({ dispatch }) {
    return Promise.all([
      dispatch(
        'map/setOverlay',
        {
          text: "Sun: General Loon! What was that noise? Everything okay with you?",
        actions: [
          {name: `shadow-world/SWITCH_3/wonder`, text: 'continue' }
        ]
      },
        { root: true }),
      dispatch('map/hideLayer', "BARRIER_4", { root: true }),
    ])
  },

  wonder({dispatch}){
    dispatch(
    'map/setOverlay',
      {
        text: "Loon: Tss, are you worried about me? Interesting.",
        actions: [
          { name: `shadow-world/SWITCH_3/ignore`, text: 'continue' }
        ]
      },
      { root: true }
  )
  }

}
