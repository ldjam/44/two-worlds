export default {
  activate({ dispatch }, { routeName, layerName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: A barrier? What happened here? And where is my dear princess?",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  }
}
