export default {
  activate({ dispatch }, { routeName, layerName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: A barrier. But I can not find a switch anywhere. Maybe it is in the world of this girl ...\n" +
        "Hey girl, do something!",
        actions: [
          { name: `${routeName}/${layerName}/say`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  say({ dispatch }) {
    dispatch('map/setOverlay', {
      text: "My name is Sun! The old wise lady said you have to press T on your keyboard so I can help you in my world.",
      actions: [
        { name: `shadow-world/BARRIER_1/ignore`, text: 'continue' }
      ]
    }, { root: true })
  }


}
