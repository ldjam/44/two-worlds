export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: 'Hey! I sell things. Click "Remove" to remove me.',
        actions: [
          {
            name: `${routeName}/${layerName}/remove`,
            text: 'Remove',
            payload: { layerName }
          }
        ]
      },
      { root: true }
    )
  },

  remove({ dispatch }, { layerName }) {
    dispatch('map/setOverlay', null, { root: true }).then(() =>
      dispatch('map/hideLayer', layerName, { root: true })
    )
  }
}
