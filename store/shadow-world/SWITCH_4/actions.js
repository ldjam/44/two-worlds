export default {
  activate({ dispatch }, { layerName, routeName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: Hm? This lamp does not light up ... wait ... it looks similar to the other lamps, " +
        "but... something is different... hm...here is a switch..",
        actions: [
          { name: `${routeName}/${layerName}/ignore`, text: 'Do not press the switch.' },
          { name: `${routeName}/${layerName}/press`, text: 'Press switch.' }
        ]
      },
      { root: true }
    )
  },

  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  press({ dispatch }) {
    return Promise.all([
      dispatch(
        'map/setOverlay',
        {
          text: "Loon: The barrier has disappeared ... that was probably the technique for the barrier ... " +
          "Most interesting.",
        actions: [
          {name: `shadow-world/SWITCH_4/wonder`, text: 'continue' }
        ]
      },
        { root: true }),
      dispatch('map/hideLayer', "BARRIER_0", { root: true }),
    ]
    )
  },

  wonder({dispatch}){
    dispatch(
    'map/setOverlay',
      {
        text: "Sun: General Loon! The barrier has disappeared in my World too!",
        actions: [
          { name: `shadow-world/SWITCH_4/ignore`, text: 'continue' }
        ]
      },
      { root: true }
  )
  }

}
