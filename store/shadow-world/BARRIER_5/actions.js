export default {
  activate({ dispatch }, { routeName, layerName }) {
    dispatch(
      'map/setOverlay',
      {
        text: "Shadow Soldier: General Loon! Thank the gods you are here! We were suddenly attacked by these death gods. " +
        "They kidnapped the princess and killed our king. " +
        "The soldiers are still fighting bravely, but we need your strength.",
        actions: [
          { name: `${routeName}/${layerName}/say`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  ignore({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })
  },

  say({ dispatch }) {
    dispatch('map/setOverlay', {
      text: "Loon: Our King?! Where are those with the princess?",
      actions: [
        { name: `shadow-world/BARRIER_5/sayAgain`, text: 'continue' }
      ]
    }, { root: true })
  },

  sayAgain({ dispatch }) {
    dispatch('map/setOverlay', {
      text: "Shadow Soldier: They ran towards the west tower. I couldn't stop them, forgive me, General.",
      actions: [
        { name: `shadow-world/BARRIER_5/sayAgainAndAgain`, text: 'continue' }
      ]
    }, { root: true })
  },

  sayAgainAndAgain({ dispatch }) {
    dispatch('map/setOverlay', {
      text: "Loon: Don't worry. You have certainly done your best. I'll bring the princess back.",
      actions: [
        { name: `shadow-world/BARRIER_5/ignore`, text: 'continue' }
      ]
    }, { root: true })
  },



}
