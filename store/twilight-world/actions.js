export default {
  next({ dispatch }) {
    dispatch(
      'map/setOverlay',
      {
        text: 'Old Wise Woman: Sun, my girl. General Loon.',
        actions: [{ name: 'twilight-world/speech1', text: 'continue' }]
      },
      { root: true }
    )
  },

  speech1({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Sun: How do you know my name?",
        actions: [
          { name: `twilight-world/speech2`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },

  speech2({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Old Wise Woman: Hihihihi, I know all about the world, my girl.",
        actions: [
          { name: `twilight-world/speech3`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },

  speech3({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: Then you can certainly tell us where we are here.",
        actions: [
          { name: `twilight-world/speech4`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },

  speech4({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Old wise woman: General Loon, listen carefully. " +
        "You are in the twilght world. It is a time-less and space-less world and connects your two worlds together.",
        actions: [
          { name: `twilight-world/speech5`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },

  speech5({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: I do not care. Bring me back I have to find my princess.",
        actions: [
          { name: `twilight-world/speech6`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  speech6({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Old Wise Woman: You should care about it, General. " +
        "The Death Gods have invaded your two worlds to steal life. " +
        "In your world, Sun, the gods exchange your years of life defined by birth with earthly goods. " +
        "In your world, General Loon, they ruthlessly mow their lives.",
        actions: [
          { name: `twilight-world/speech7`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  speech7({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: Then we'll beat them all back.",
        actions: [
          { name: `twilight-world/speech8`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  speech8({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Old Wise Woman: It's not that easy. " +
        "They have kidnapped your sister, Sun, and the princess" +
        " because they have special powers in them that bring the death gods to power.",
        actions: [
          { name: `twilight-world/speech9`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  speech9({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Sun: Death Gods kidnapped my sister ?! How can I save her, old lady?",
        actions: [
          { name: `twilight-world/speech10`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  speech10({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Old Wise Woman: You need to work together Sun and General Loon.",
        actions: [
          { name: `twilight-world/speech11`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  speech11({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Loon: I should work with such a child?! Ridiculous.",
        actions: [
          { name: `twilight-world/speech12`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  speech12({dispatch}){
    dispatch(
      'map/setOverlay',
      {
        text: "Old Wise Woman: You need you. If one of you gets stuck, the T key changes the worlds. " +
        "There is a switch in your world, General Loon, that makes the barrier disappear. " +
        "The one lamp that is not lit. Good luck you two.",
        actions: [
          { name: `twilight-world/finish`, text: 'continue' }
        ]
      },
      { root: true }
    )
  },


  finish({ dispatch }) {
    dispatch('map/setOverlay', null, { root: true })

    const { router } = this.app
    router.push('/light-world')
  }
}
