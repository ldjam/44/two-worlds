import { Howl } from 'howler'

const playSound = soundName => {
  if (!localStorage || localStorage.isSoundEnabled === 'false') {
    return
  }

  const plingSound = new Howl({
    src: [`sounds/${soundName}.ogg`, `sounds/${soundName}.mp3`]
  })
  plingSound.play()
}

export default playSound
